# Linode Arch 

[[_TOC_]]

Presently testing to make an Arch installation for Linode via Terraform.

## Setup

1. Have Terraform installed
1. Set up variables file. 

### Variables file 

Name of file: `variables.tf`

Example Contents:
```terraform
variable "linodeapikey" {
  default = "some_long_linode_apikey"
}

variable "archrootpass" {
  default = "the_most_secure_root_password_random_i_swear"
}

variable "ssh_key" {
	description = "SSH Public Key"
	default = "~/.ssh/someKeyForLinode.pub"
}

variable "linode_username" {
  description = "Username to be used as noted in the Linode profile"
  default = "linodiest"
}
```

## Example use 

1. `terraform init`
1. `terraform plan`
1. `terraform apply`
1. `terraform destroy`
