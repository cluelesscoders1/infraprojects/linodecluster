K3s-ALARM
=========

Installs K3s on an ALARM cluster.

Requirements
------------

Bootstrap role must be run prior. Insure that a mainline kernel and uboot has been installed on all the nodes. 


Role Variables
--------------

N/A

Dependencies
------------

* The bootstrap role must be run prior for the basics. 
* Inventory must have a masters and slaves inventory group. 

Example Playbook
----------------

```yaml
---
# Playbook to install, activate, and join nodes to the master. 

- hosts: all
  gather_facts: false
  tasks:  
    - import_role:
        name: k3s-alarm
      tags: 
        - install
```

License
-------


Author Information
------------------

Jason Plum and William Christensen... TODO: Add more details here.  
