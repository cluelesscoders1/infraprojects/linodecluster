#!/bin/bash
# A grabs the kubeconfig from the master node and setups up the kubectl locally. 
ansible masters[0] -m fetch -a "src=/etc/rancher/k3s/k3s.yaml dest=~/.kube/config flat=yes" --become
MASTER_URL=$(grep master_url linode.hosts)
ansible localhost -m lineinfile -a "path=~/.kube/config regexp='    server: https://127.0.0.1:6443' line='    server: {{ master_url }}' state=present" -e "$MASTER_URL"
