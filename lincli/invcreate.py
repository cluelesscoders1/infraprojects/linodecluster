import sys,json,hcl

workerName = 'k3sworker'
masterName = 'k3smaster'
workerGroup = 'workers'
masterGroup = 'masters'

def generateInventory(inventory_dict):
	"""Generates an ansible inventory for a k3s cluster form Linode-cli with --json option
	inventory_dict: dictionary systems with label and ip keys.
	returns: string representation of an Ansible inventory by group with host name and ip"""
	try:
		retval = ""
		for group in inventory_dict.keys():
			retval += f"\n[{group}]\n"	
			for groupsys in inventory_dict[group]:
				retval += f"{groupsys['label']} ansible_ssh_host={groupsys['ip']}\n"
		return retval

	except:
		raise

def readLinodes(linodesJson):
	"""Read Linode output from: linode-cli linodes list --json
	linodesJson: json file inventory file.
	returns: Dictionary of the inventory broken up into globally defined groups prefix names"""
	try:
		jdata = {}
		dict_inventory = {masterGroup:[], workerGroup:[]}

		lenworker = len(workerName)
		lenmaster = len(masterName)

		with open(linodesJson,"r") as jfile:
			jdata = json.load(jfile)

		for somesystem in jdata:
			if somesystem['label'][:lenworker] == workerName:
				dict_inventory[workerGroup].append({'label':somesystem['label'], 'ip':somesystem['ipv4'][0]}) 
			elif somesystem['label'][:lenmaster] == masterName:
				dict_inventory[masterGroup].append({'label':somesystem['label'], 'ip':somesystem['ipv4'][0]}) 
		return dict_inventory
	except:
		raise

def generateMasterUrl(inventory_dict):
	"""Read system names and set the master url to the IP address
	inventory_dict: dictionary systems with label and ip keys.
	returns: string for the master_url variable in an ansible inventory"""
	try:
		retval = "\n# Cluster Variable\n"
		master_ip = inventory_dict[masterGroup][0]['ip']
		retval += f"master_url=https://{ master_ip }:6443\n"	
		return retval
	except IndexError as e:
		retval = "\n# Cluster Variable\n"
		retval += f"master_url=https://master1:6443\n"	
		return retval
	except:
		raise

def readTerraformVariables(tfVarFile):
	"""Read Terraform Variables file
	tfVarFile: terraform formated file written to the use the variables names in the readme
	returns: dictionary output from the hcl library"""
	try:
		tfdata = {}
		with open(tfVarFile,'r') as tfFile:
			tfdata = hcl.loads(tfFile.read())
		return tfdata
	except:
		raise

def generateHostVariables(terraform_dict):
	"""Take the output from the readTerraformVariables function. Assuming the variables
	defined as specified in the readme, the login information for ansible is generated.
	terraform_dict: hcl output of the terraform variables file for Linode
	returns: string of ansible variables to be written to a file"""
	try:
		retval = "\n# Ansible Login information\n"
		retval += f"ansible_user={terraform_dict['variable']['kubernetes_user_name']['default']}\n"
		retval += f"ansible_ssh_pass={terraform_dict['variable']['kubernetes_user_password']['default']}\n"
		retval += f"ansible_become_pass={terraform_dict['variable']['kubernetes_user_password']['default']}\n"
		retval += f"ansible_become_method=sudo\n"
		return retval
	except:
		raise

def main(args):
	outFileHosts = None  # outputs machine names 
	inLinodeJson = None  # json file pulled from the the linode cli.
	inTerraVars  = None  # terraform variables file for creating inventory  
	outFileLogin = None  # outputs login names from terraform variables 
	if len(args) < 2:
		raise ValueError("Need 2 or 3 arguments to use this fucntion")
	elif len(args) >= 2: 
		inLinodeJson = args[1]
	if len(args) >= 3:
		outFileHosts = args[2]
	if len(args) >= 4:
		inTerraVars = args[3]
	if len(args) >= 5:
		outFileLogin = args[4]

	try:
		dict_inventory = readLinodes(inLinodeJson)

		retHosts = generateInventory(dict_inventory)
		retval = retHosts

		if outFileHosts == None:
			return retval
		else:
			with open(outFileHosts,"w") as outfile:
				outfile.write(retHosts)
				outfile.close()
			# return retval

		retVars = generateMasterUrl(dict_inventory)
		dict_terraformVars = None
		if inTerraVars != None:
			dict_terraformVars = readTerraformVariables(inTerraVars)
			retVars += generateHostVariables(dict_terraformVars)
			retval += retVars
			with open(outFileLogin,"w") as outfile:
				outfile.write(retVars)
				outfile.close()

		if outFileLogin == None or outFileHosts == None:
			return retval

	except:
		raise

if __name__ == "__main__":
	output = main(sys.argv)
	if output != None:
		print(output)
