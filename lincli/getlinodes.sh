#!/usr/bin/bash
source bin/activate
linode-cli linodes list --json > listjson.json 
python invcreate.py listjson.json ../ansible/linodeinv.hosts ../tf/variables.tf ../ansible/linodelogin.hosts
cat ../ansible/linodeinv.hosts ../ansible/base.hosts ../ansible/linodelogin.hosts > ../ansible/linode.hosts
